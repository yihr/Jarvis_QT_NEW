#include "jarvisspeech.h"
JarvisSpeech::JarvisSpeech()
{
    m_type = Type::SPEECH;
}

void JarvisSpeech::Init(QString resPath)
{

}

QWidget *JarvisSpeech::GetWidget(QWidget *parent)
{
    tab = new SpeechWidget;
    connect(tab,&SpeechWidget::ChangeState,this,&JarvisSpeech::ChangeState);
    connect(tab,&SpeechWidget::ReceiveReply,this,&JarvisSpeech::ReceiveReply);
    connect(tab,&SpeechWidget::ButtonState,this,&JarvisSpeech::ButtonState);
    connect(tab,&SpeechWidget::ReceiveSTT,this,&JarvisSpeech::ReceiveSTT);
    return tab;
}

void JarvisSpeech::PollMessage(JarvisInterface::Type from, QJsonObject data)
{
    if(from == Type::AGENT){
        if(data["MODE"] == "Click")emit tab->on_pushButton_clicked();
    }
}

JarvisSpeech::~JarvisSpeech()
{
    delete tab;
}

void JarvisSpeech::ChangeState(bool state)
{
    QJsonObject obj;
    obj["MODE"] = "ChangeMonth";
    obj["Audio_State"] = state;
    emit SendData(Type::AGENT,obj);
}

void JarvisSpeech::ReceiveReply(QJsonObject data)
{
    QJsonObject obj;
    obj["MODE"] = "AIUI";
    obj["data"] = data;
    emit SendData(Type::OTHER,obj);
}

void JarvisSpeech::ReceiveSTT(QString data)
{
    QJsonObject obj;
    obj["MODE"] = "ReceiveSTT";
    obj["data"] = data;
    emit SendData(Type::AGENT,obj);
}

void JarvisSpeech::ButtonState(bool state)
{
    QJsonObject obj;
    obj["MODE"] = "ButtonState";
    obj["state"] = state;
    emit SendData(Type::AGENT,obj);
}
