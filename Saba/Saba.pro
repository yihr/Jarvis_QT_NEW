#-------------------------------------------------
#
# Project created by QtCreator 2019-07-24T09:57:24
#
#-------------------------------------------------

QT       -= gui

TARGET = Saba
TEMPLATE = lib
CONFIG += staticlib
CONFIG += c++14 gnu++14
# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += \
    ../External/glm/include \
    ../External/spdlog/include \
    ../External/tinyddsloader/include \
    ../External/tinyxfileloader/include \
    ../External/tinyobjloader/include \
    ../Bullet/src

SOURCES += \
    Saba/Base/File.cpp \
    Saba/Base/Log.cpp \
    Saba/Base/Path.cpp \
    Saba/Base/Singleton.cpp \
    Saba/Base/Time.cpp \
    Saba/Base/UnicodeUtil.cpp \
    Saba/Model/MMD/MMDCamera.cpp \
    Saba/Model/MMD/MMDIkSolver.cpp \
    Saba/Model/MMD/MMDMaterial.cpp \
    Saba/Model/MMD/MMDModel.cpp \
    Saba/Model/MMD/MMDMorph.cpp \
    Saba/Model/MMD/MMDNode.cpp \
    Saba/Model/MMD/MMDPhysics.cpp \
#    Saba/Model/MMD/PMDFile.cpp \
#    Saba/Model/MMD/PMDModel.cpp \
    Saba/Model/MMD/PMXFile.cpp \
    Saba/Model/MMD/PMXModel.cpp \
    Saba/Model/MMD/SjisToUnicode.cpp \
    Saba/Model/MMD/VMDAnimation.cpp \
    Saba/Model/MMD/VMDCameraAnimation.cpp \
    Saba/Model/MMD/VMDFile.cpp
#    Saba/Model/MMD/VPDFile.cpp \
#    Saba/Model/OBJ/OBJModel.cpp \
#    Saba/Model/OBJ/tinyobjloader.cpp \
#    Saba/Model/XFile/XFileModel.cpp

HEADERS += \
    Saba/Base/File.h \
    Saba/Base/Log.h \
    Saba/Base/Path.h \
    Saba/Base/Singleton.h \
    Saba/Base/Time.h \
    Saba/Base/UnicodeUtil.h \
    Saba/Model/MMD/MMDCamera.h \
    Saba/Model/MMD/MMDFileString.h \
    Saba/Model/MMD/MMDIkSolver.h \
    Saba/Model/MMD/MMDMaterial.h \
    Saba/Model/MMD/MMDModel.h \
    Saba/Model/MMD/MMDMorph.h \
    Saba/Model/MMD/MMDNode.h \
    Saba/Model/MMD/MMDPhysics.h \
#    Saba/Model/MMD/PMDFile.h \
#    Saba/Model/MMD/PMDModel.h \
    Saba/Model/MMD/PMXFile.h \
    Saba/Model/MMD/PMXModel.h \
    Saba/Model/MMD/SjisToUnicode.h \
    Saba/Model/MMD/VMDAnimation.h \
    Saba/Model/MMD/VMDAnimationCommon.h \
    Saba/Model/MMD/VMDCameraAnimation.h \
    Saba/Model/MMD/VMDFile.h
#    Saba/Model/MMD/VPDFile.h
#    Saba/Model/OBJ/OBJModel.h \
#    Saba/Model/XFile/XFileModel.h
unix {
    target.path = /usr/lib
    INSTALLS += target
}
