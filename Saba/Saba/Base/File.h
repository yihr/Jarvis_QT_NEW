﻿//
// Copyright(c) 2016-2017 benikabocha.
// Distributed under the MIT License (http://opensource.org/licenses/MIT)
//

#ifndef SABA_BASE_FILE_H_
#define SABA_BASE_FILE_H_

#include <QFile>
#include <QDebug>
#include <QIODevice>
#include <QDataStream>
#include <cstring>

namespace saba
{
	class File
	{
    public:
        File() {}
		File(const File&) = delete;
		File& operator = (const File&) = delete;

		bool Open(const char* filepath);
        bool Open(std::string filepath);

        void Close();

        bool IsBad();
        qint64 GetSize();
        qint64 Tell();

		template <typename T>
		bool Read(T* buffer, size_t count = 1)
		{
           m_file.read((char *)buffer,count*sizeof(T));
           return true;
		}
    private:
        QFile m_file;
        bool m_bad = false;
	};
}

#endif // !BASE_FILE_H_
