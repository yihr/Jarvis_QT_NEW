﻿//
// Copyright(c) 2016-2017 benikabocha.
// Distributed under the MIT License (http://opensource.org/licenses/MIT)
//

#include "File.h"
#include "UnicodeUtil.h"

namespace saba
{
    bool File::Open(const char *filepath)
    {
        m_file.setFileName(filepath);
        return m_file.open(QIODevice::ReadOnly);
    }

    bool File::Open(std::string filepath)
    {
       return Open(filepath.c_str());
    }

    void File::Close()
    {
        m_file.close();
    }

    bool File::IsBad()
    {
        return m_bad;
    }

    qint64 File::GetSize()
    {
        return m_file.size();
    }

    qint64 File::Tell()
    {
        return m_file.pos();
    }
}

