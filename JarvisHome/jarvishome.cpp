#include "jarvishome.h"


JarvisHome::JarvisHome()
{
    m_type = Type::OTHER;
}

void JarvisHome::Init(QString resPath)
{

}

QWidget *JarvisHome::GetWidget(QWidget *parent)
{
    QWidget *tab = new QWidget();
    ui = new Ui::Form;
    ui->setupUi(tab);
    connect(ui->pushButton,&QPushButton::clicked,this,&JarvisHome::on_pushButton_clicked);
    client = new QTcpSocket(this);
    client->abort();
    client->connectToHost(ui->lineEdit->text(),333);
    return tab;
}

void JarvisHome::PollMessage(JarvisInterface::Type from, QJsonObject data)
{
    if(from == Type::SPEECH){
        if(data["MODE"] == "AIUI"){
            data = data["data"].toObject()["data"].toArray()[0].toObject();
            QString ret = data["intent"].toObject()["semantic"].toObject()["slots"].toObject()["attrValue"].toString();
            if(ret == "开"){
                ChangeState(true);
            }else{
                ChangeState(false);
            }
        }
    }
}

JarvisHome::~JarvisHome()
{
    delete ui;
}

void JarvisHome::ChangeState(bool state)
{
    QString msg = state?ui->lineEdit_2->text():ui->lineEdit_3->text();
    msg+="\n";
    client->write(msg.toLatin1());
    this->state = state;
}

void JarvisHome::on_pushButton_clicked()
{
    ChangeState(state = !state);
}
