#include "mmdshader.h"

MMDShader::MMDShader(QString vertexFile,QString fragFile){
    mmdShader.addShaderFromSourceFile(QOpenGLShader::Vertex,vertexFile);
    mmdShader.addShaderFromSourceFile(QOpenGLShader::Fragment,fragFile);
    mmdShader.link();
    mmdShader.bind();

    // attribute
    m_inPos = mmdShader.attributeLocation("in_Pos");
    m_inNor = mmdShader.attributeLocation("in_Nor");
    m_inUV = mmdShader.attributeLocation("in_UV");

    // uniform
    m_uWV = mmdShader.uniformLocation("u_WV");
    m_uWVP = mmdShader.uniformLocation("u_WVP");

    m_uAmbinet = mmdShader.uniformLocation("u_Ambient");
    m_uDiffuse = mmdShader.uniformLocation("u_Diffuse");
    m_uSpecular = mmdShader.uniformLocation("u_Specular");
    m_uSpecularPower = mmdShader.uniformLocation("u_SpecularPower");
    m_uAlpha = mmdShader.uniformLocation("u_Alpha");

    m_uTexMode = mmdShader.uniformLocation("u_TexMode");
    m_uTex = mmdShader.uniformLocation("u_Tex");
    m_uTexMulFactor = mmdShader.uniformLocation("u_TexMulFactor");
    m_uTexAddFactor = mmdShader.uniformLocation("u_TexAddFactor");

    m_uSphereTexMode = mmdShader.uniformLocation("u_SphereTexMode");
    m_uSphereTex = mmdShader.uniformLocation("u_SphereTex");
    m_uSphereTexMulFactor = mmdShader.uniformLocation("u_SphereTexMulFactor");
    m_uSphereTexAddFactor = mmdShader.uniformLocation("u_SphereTexAddFactor");

    m_uToonTexMode = mmdShader.uniformLocation("u_ToonTexMode");
    m_uToonTex = mmdShader.uniformLocation("u_ToonTex");
    m_uToonTexMulFactor = mmdShader.uniformLocation("u_ToonTexMulFactor");
    m_uToonTexAddFactor = mmdShader.uniformLocation("u_ToonTexAddFactor");

    m_uLightColor = mmdShader.uniformLocation("u_LightColor");
    m_uLightDir = mmdShader.uniformLocation("u_LightDir");

    m_uLightVP = mmdShader.uniformLocation("u_LightWVP");
}

QOpenGLShaderProgram & MMDShader::get(){
    return mmdShader;
}
