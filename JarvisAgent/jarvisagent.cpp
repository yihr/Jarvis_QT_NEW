#include "jarvisagent.h"

JarvisAgent::JarvisAgent()
{
    m_type = Type::AGENT;
}

void JarvisAgent::Init(QString resPath)
{
    this->resPath = resPath;

}

QWidget *JarvisAgent::GetWidget(QWidget *parent)
{
    tab = new QWidget();;
    QVBoxLayout *verticalLayout = new QVBoxLayout(tab);
    verticalLayout->setSpacing(6);
    verticalLayout->setContentsMargins(9, 9, 9, 9);
    verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
    verticalLayout->addWidget(agent = new AgentGLWidget(resPath,tab));
    agent->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    button = new QPushButton(tab);
    connect(button,&QPushButton::clicked,this,&JarvisAgent::ButtonClick);
    button->setText("开始对话");
    verticalLayout->addWidget(button);
    label = new QLabel(tab);
    label->setText("这里是状态信息。");
    verticalLayout->addWidget(label);
    return tab;
}

void JarvisAgent::PollMessage(JarvisInterface::Type from, QJsonObject data)
{
    if(from == Type::SPEECH){
        if(data["MODE"].toString() == "ChangeMonth")agent->ChangeMonth(data["Audio_State"].toBool());
        else if(data["MODE"].toString() == "ReceiveSTT")label->setText(data["data"].toString());
        else if(data["MODE"].toString() == "ButtonState"){
            buttonState = data["state"].toBool();
            button->setText(buttonState?"开始录音":"结束录音");
        }
    }
}

JarvisAgent::~JarvisAgent()
{
    if(tab != nullptr) delete tab;
}

void JarvisAgent::ButtonClick()
{
    QJsonObject obj;
    obj["MODE"] = "Click";
    emit SendData(JarvisAgent::Type::SPEECH,obj);
}
