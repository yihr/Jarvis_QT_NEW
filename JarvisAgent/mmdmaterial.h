#ifndef MMDMATERIAL_H
#define MMDMATERIAL_H

#include <Saba/Model/MMD/MMDMaterial.h>
#include <QOpenGLTexture>

class Material{
public:
    explicit Material(saba::MMDMaterial mat);
public:
    saba::MMDMaterial m_mmdMat;
    QOpenGLTexture *m_texture;
    //bool	m_textureHasAlpha = false;
    QOpenGLTexture *m_spTexture;
    QOpenGLTexture *m_toonTexture;
private:

};

#endif // MMDMATERIAL_H
