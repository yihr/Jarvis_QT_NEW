#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QDir>
#include <QPluginLoader>
#include <QDebug>
#include <QJsonDocument>
#include "../JarvisInterface/jarvisinterface.h"

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = nullptr);
    ~Widget();
private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();
    void SendData(JarvisInterface::Type to, QJsonObject data);
    void on_pushButton_3_clicked();

private:
    void ReleaseAllInterface();
    void InitPlugin(JarvisInterface * mylib,QString tabName);
private:
    Ui::Widget *ui;
    QStringList pluginList;
    QList<JarvisInterface *> interfaces;
    QDir resDir;
};

#endif // WIDGET_H
