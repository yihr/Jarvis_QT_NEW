#ifndef TESTINTERFACE_H
#define TESTINTERFACE_H

#include "jarvisinterface.h"
#include <QDebug>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include <QtWidgets/QPushButton>
class TestInterface : public JarvisInterface
{
    Q_OBJECT
#ifdef Q_OS_ANDROID

#else
    Q_PLUGIN_METADATA(IID "com.finch.testlib" FILE "test.json")
    Q_INTERFACES(JarvisInterface)
#endif

public:
    TestInterface();
    void Init(QString resPath);
    QWidget * GetWidget(QWidget * parent);
    void PollMessage(Type from,QJsonObject data);
    ~TestInterface();
private:
    QWidget *tab;
    QVBoxLayout *verticalLayout_2;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
};

#endif // TESTINTERFACE_H
